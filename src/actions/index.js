export const changeSortByFilter = text =>({
    type: 'CHANGE_SORT_BY',
    text
});

export const changeOrderByFilter = text =>({
    type: 'CHANGE_ORDER_BY',
    text
});

export const changeSearchByFilter = text =>({
    type: 'CHANGE_SEARCH_BY',
    text
});
