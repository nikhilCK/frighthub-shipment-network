import React, {Component} from 'react';
import {connect} from "react-redux";

import ButtonSolid from "../../shared/components/Buttons/ButtonSolid/ButtonSolid";
import Dropdown from "../../shared/components/Dropdown/Dropdown";
import Label from "../../shared/components/Label/Label";
import SearchBar from "../../shared/components/SearchBar/SearchBar";
import SimpleTable from "../../shared/components/SimpleTable/SimpleTable";
import Loader from "../../shared/components/SpinnerLoader/SpinnerLoader";

import {getShipmentList} from "./ShipmentListService";
import {changeOrderByFilter, changeSearchByFilter, changeSortByFilter} from "../../actions";

import './styles.scss';


class ShipmentList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fetchApiStatus: 1,
            pagination: {},
            pageData: []
        };
        this.searchConstraint = '';
        this.pageLimit = 20;
        this.pageNumber = 1;
        this.navButtonCustomStyle = {
            width: '60px',
            height: '30px',
            margin: '0 10px',
            padding: '5px'
        };
        this.cols = ['id', 'name', 'destination', 'mode', 'origin', 'status', 'total', 'type'];
        this.options = [];
        this.sortOptions = [];
        this.orderOptions = [];
    }

    componentWillMount() {
        this.options = [
            {value: 'id', label: 'Id'},
            {value: 'name',label: 'Shipment Name'},
            {value: 'destination', label: 'Destination'},
            {value: 'mode', label: 'Transport Mode'},
            {value: 'origin', label: 'Origin'}
        ];
        this.sortOptions = [
            {value: '', label: 'None'},
            {value: 'id', label: 'Id'},
            {value: 'name', label: 'Name'},
            {value: 'destination', label: 'Destination'},
            {value: 'mode', label: 'Mode'},
            {value: 'origin', label: 'Origin'},
            {value: 'status', label: 'Status'},
            {value: 'total', label: 'Total'},
            {value: 'type', label: 'Type'}
        ];
        this.orderOptions = [
            {value: 'asc', label: 'Ascending'},
            {value: 'desc', label: 'Descending'}
        ]
    }

    componentDidMount() {
        this.getShipmentFreshList();
    }

    successCallback = (response) => {
        this.setState({
            fetchApiStatus: 2,
            pageData: response.data,
            pagination: response.pagination
        })
    };

    failureCallback = (error) => {
        this.setState({
            fetchApiStatus: 3,
        });
    };

    getShipmentFreshList = () => {
        const filterObject = {
            '_page': this.pageNumber,
            '_limit': this.pageLimit,
            '_sort': this.props.sortBy,
            '_order': this.props.orderBy
        };
        this.setState({
            fetchApiStatus: 1,
        });

        if (this.searchConstraint) {
            filterObject[`${this.props.searchBy}_like`] = this.searchConstraint
        }

        getShipmentList(filterObject, this.successCallback, this.failureCallback)
    };

    getShipmentListPage = (filterObj) => {
        this.setState({
            fetchApiStatus: 1,
        });
        getShipmentList(filterObj, this.successCallback, this.failureCallback)
    };

    handlePagination = (action) => {
        this.getShipmentListPage(this.getQueryStringParameters(this.state.pagination[action]))
    };

    openDetails = (id) => {
        this.props.history.push('/details/' + id);
    };

    searchShipment = (constraint) => {
        this.searchConstraint = constraint;
        this.getShipmentFreshList();
    };

    searchByChanged = (value) => {
        this.props.changeSearchBy(value);
        if (this.searchConstraint) {
            this.getShipmentFreshList();
        }
    };

    sortByChanged = (value) => {
        this.props.changeSortBy(value);
        this.getShipmentFreshList();
    };

    orderByChanged = (value) => {
        this.props.changeOrderBy(value);
        this.getShipmentFreshList();
    };


    /**
     * Reference - https://www.arungudelli.com/tutorial/javascript/get-query-string-parameter-values-from-url-using-javascript/
     * Takes url as parameter and returns
     * parameters name and value in JSON key value format
     * @parameter {String} url
     **/
    getQueryStringParameters = url => {
        let query = '';
        if (url) {
            if (url.split("?").length > 0) {
                query = url.split("?")[1];
            }
        }
        return (/^[?#]/.test(query) ? query.slice(1) : query)
            .split('&')
            .reduce((params, param) => {
                let [key, value] = param.split('=');
                params[key] = value ? decodeURIComponent(value.replace(/\+/g, ' ')) : '';
                return params;
            }, {});
    };

    render() {
        const {pagination, pageData , fetchApiStatus} = this.state;
        const{ searchBy, orderBy, sortBy} = this.props;
        return (
            <div className='shipment-list-container'>
                <div className='filter-container'>
                    <Dropdown
                        onSelectionDone={this.searchByChanged}
                        options={this.options}
                        selectedValue={searchBy}
                        customStyle={{
                            'width': '150px',
                            'marginRight': '10px'
                        }}
                    />
                    <SearchBar
                        className="bordered"
                        width="300px"
                        placeholder="Search by Id"
                        onSearch={this.searchShipment}
                    />
                </div>
                <div className='filter-container'>
                    <Label
                        text={'Sort By'}
                    />
                    <Dropdown
                        onSelectionDone={this.sortByChanged}
                        options={this.sortOptions}
                        selectedValue={sortBy}
                        customStyle={{
                            'width': '150px',
                            'marginRight': '10px'
                        }}
                    />
                    <Label
                        text={'Order By'}
                    />
                    <Dropdown
                        onSelectionDone={this.orderByChanged}
                        options={this.orderOptions}
                        selectedValue={orderBy}
                        customStyle={{
                            'width': '150px',
                            'marginRight': '10px'
                        }}
                    />
                </div>

                <SimpleTable
                    onRowClick={(row) => {
                        this.openDetails(row.id);
                    }}
                    columns={this.cols}
                    rows={pageData}
                    customStyle={{
                        'cursor': 'pointer'
                    }}
                />
                <div className='buttons-container'>
                    <ButtonSolid
                        customStyle={this.navButtonCustomStyle}
                        isDisabled={!pagination['prev']}
                        onClick={() => this.handlePagination('first')}
                        title={'First'}/>
                    <ButtonSolid
                        customStyle={this.navButtonCustomStyle}
                        isDisabled={!pagination['prev']}
                        onClick={() => this.handlePagination('prev')}
                        title={'Prev'}/>
                    <ButtonSolid
                        customStyle={this.navButtonCustomStyle}
                        isDisabled={!pagination['next']}
                        onClick={() => this.handlePagination('next')}
                        title={'Next'}/>
                    <ButtonSolid
                        customStyle={this.navButtonCustomStyle}
                        isDisabled={!pagination['next']}
                        onClick={() => this.handlePagination('last')}
                        title={'Last'}/>
                </div>
                {fetchApiStatus === 1 ? <Loader key={'loading'}/> : null}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        searchBy: state.appliedFilters.searchBy,
        orderBy: state.appliedFilters.orderBy,
        sortBy: state.appliedFilters.sortBy
    };
}

function mapDispatchToProps(dispatch, ownProps) {
    return {
        changeSortBy: (text) => dispatch(changeSortByFilter(text)),
        changeOrderBy: (text) => dispatch(changeOrderByFilter(text)),
        changeSearchBy: (text) => dispatch(changeSearchByFilter(text))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ShipmentList);
