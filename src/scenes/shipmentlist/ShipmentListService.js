import axios from 'axios';

export function getShipmentList(filterObject, callbackSuccess, callbackFailure){

    axios({
        method:'get',
        url:'http://localhost:3000/shipments',
        params: filterObject
    }).then((response)=>{
        let paginationObject = {};
        if(response.headers.link){
            let linkArray = response.headers.link.split(',')
            linkArray.forEach( item =>{
                item.trim();
                let link = item.split(';');
                let key = link[1].split('=')[1].trim().replace(/["]/g, "");
                paginationObject[key] = link[0].replace(/[<>]/g, "");
            });
        }
        callbackSuccess({
            data : response.data,
            pagination :paginationObject
        });

    }).catch((error)=>{
        callbackFailure(error)
    })
}
