import axios from 'axios';

export function getShipmentDetails(id, callbackSuccess, callbackFailure){
    axios({
        method:'get',
        url:'http://localhost:3000/shipments/' + id,
    }).then((response)=>{
        callbackSuccess(response.data);

    }).catch((error)=>{
        callbackFailure(error)
    })
}

export function updateShipmentDetails(id, data, callbackSuccess, callbackFailure){
    axios({
        method:'put',
        url:'http://localhost:3000/shipments/' + id,
        data: data
    }).then((response)=>{
        callbackSuccess(response.data);

    }).catch((error)=>{
        callbackFailure(error)
    })
}
