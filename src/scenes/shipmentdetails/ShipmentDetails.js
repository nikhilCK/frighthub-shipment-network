import React, {Component} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import ButtonIcon from "../../shared/components/Buttons/ButtonIcon/ButtonIcon";
import SimpleTable from "../../shared/components/SimpleTable/SimpleTable";
import Label from "../../shared/components/Label/Label";
import TextBox from "../../shared/components/TextBox/TextBox";

import {getShipmentDetails, updateShipmentDetails} from "./ShipmentDetailService";

import './styles.scss';
import Toast from "../../shared/components/Toast/Toast";
import Loader from "../../shared/components/SpinnerLoader/SpinnerLoader";


export default class ShipmentDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            shipmentDetails: {},
            fetchApiStatus: 1,
            inputChanged: false
        };
        this.shipmentId = null
        this.cargoColumns = ['type', 'description', 'volume']
        this.patchValues = {};
        this.toastMessage = 'Information Saved';
    }

    componentWillMount() {
        this.shipmentId = this.props.match.params.shipmentId
    }

    componentDidMount() {
        this.getShipmentDetails();
    }

    onGetDetailSuccess = (response) => {
        this.setState({
            'shipmentDetails': response,
            fetchApiStatus: 2
        })
    };

    getShipmentDetails = () => {
        this.setState({
            fetchApiStatus: 1
        });
        getShipmentDetails(this.shipmentId, this.onGetDetailSuccess,
            (error) => {
                this.setState({
                    fetchApiStatus: 3
                });
            });
    };

    updateShipmentDetails = () => {
        this.setState({
            fetchApiStatus: 1
        });
        const formData = Object.assign({}, this.state.shipmentDetails, this.patchValues )
        updateShipmentDetails(this.shipmentId, formData, (data)=>{
            this.setState({
                shipmentDetails : Object.assign({}, this.state.shipmentDetails, data ),
                inputChanged : false,
                fetchApiStatus : 2
            });
            this.toastRef.showMessage();
            },
            (error) => {
                this.setState({
                    fetchApiStatus: 3
                });
            });
    };

    onInputChange = (value, validations) => {
        this.setState({
            inputChanged : validations.hasChanged
        });

        if(validations.hasChanged) {
            this.patchValues['name'] = value
        }else{
            delete this.patchValues['name']
        }
    };

    getViewToRender = (apiStatus) => {
        let views = [];

        switch (apiStatus) {
            case 1 :
                // views.push( <div key={'loading'} className={'loader-container'}> <div className="lds-ring"><div></div><div></div><div></div><div></div></div> </div>)
                views.push(<Loader key={'loading'}/>)
                break;
            case 2:
                const {shipmentDetails, inputChanged} = this.state;
                const buttonStyle = {
                    'width': '40px',
                    'height': '40px',
                    'fontSize': '20px'
                };

                views.push(<Label key={'id'} left={'Id'} text={this.shipmentId}/>);
                views.push(
                    <div key={'name'} className='update-field'>
                        <Label left={'Name'}/>
                        <TextBox
                            className={'override'}
                            value={shipmentDetails.name}
                            onInputChange={this.onInputChange}/>
                        <ButtonIcon
                            onClick={this.updateShipmentDetails}
                            icon={<FontAwesomeIcon icon="save"/>}
                            customStyle={buttonStyle}
                            isDisabled={!inputChanged}
                        />
                    </div>
                );
                views.push(<Label key={'origin'} left={'Origin'} text={shipmentDetails.origin}/>);
                views.push(<Label key={'destination'} left={'Destination'} text={shipmentDetails.destination}/>);
                views.push(<Label key={'mode'} left={'Transport Mode'} text={shipmentDetails.mode}/>);
                views.push(<Label key={'type'} left={'Cargo Volume Type'} text={shipmentDetails.mode}/>);
                views.push(<Label key={'quantity'} left={'Total Quantity'} text={String(shipmentDetails.total)}/>);
                if (shipmentDetails.cargo) {
                    views.push(
                        <SimpleTable
                            key={'cargo-table'}
                            rows={shipmentDetails.cargo}
                            columns={this.cargoColumns}
                            customStyle={{
                                'width': 'auto'
                            }}
                        />
                    )
                }
                views.push(<Label key={'status'}  left={'Status'} text={shipmentDetails.status}/>);
                break;
            case 3:
                views.push(<div key={'error'}> Error View </div>);
                break;
        }
        return views;
    };


    render() {
        const {fetchApiStatus} = this.state;
        const buttonStyle = {
            'width': '40px',
            'height': '40px',
            'fontSize': '20px'
        };
        return (
            <div key={'detail-container'} className='shipment-detail-container'>
                {this.getViewToRender(fetchApiStatus)}
                <Toast ref={(instance) => this.toastRef = instance} message={this.toastMessage}/>
            </div>
        )
    }
}
