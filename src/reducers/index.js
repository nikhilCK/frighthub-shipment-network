import {combineReducers} from "redux";
import appliedFilters from './shipmentFilters';

export default combineReducers({
    appliedFilters
});
