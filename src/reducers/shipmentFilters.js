const defaultFilters = {
    searchBy: 'id',
    orderBy: 'asc',
    sortBy: ''
};

const appliedFilters = (state = defaultFilters, action) =>{
    switch (action.type) {
        case 'CHANGE_SORT_BY':
            return {
                ...state,
                sortBy: action.text
            };
        case 'CHANGE_ORDER_BY':
            return {
                ...state,
                orderBy: action.text
            };
        case 'CHANGE_SEARCH_BY':
            return {
                ...state,
                searchBy: action.text
            };
        default:
            return state
    }
}

export default appliedFilters;
