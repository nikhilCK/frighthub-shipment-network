import React, {Component} from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

class TextBox extends Component {

    constructor(props) {
        super(props);
        this.wrapperClasses = "text-view-parent " + this.props.className;
    }

    componentDidMount() {
        if (this.props.value !== null && this.props.value !== undefined && String(this.props.value) !== "") {
            this.inputBox.value = this.props.value;
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if(this.props.value !== nextProps.value)
        {
            this.detectChange(this.inputBox.value, nextProps)
        }
    }

    detectChange = (value, props = this.props) => {
        this.props.onInputChange(value, {
            hasChanged: (value !== props.value)
        })
    };

    updateInputValue = (event) => {
        this.detectChange(event.target.value)
    };

    render() {
        const {placeholder} = this.props;
        return (
            <div className={this.wrapperClasses} >
                <input
                    ref={(val) => this.inputBox = val}
                    type="input"
                    className="text-box-input"
                    placeholder={placeholder ? placeholder : 'P'}
                    onChange={this.updateInputValue}
                />
            </div>
        );
    }
}

TextBox.propTypes = {
    placeholder: PropTypes.string,
    value: PropTypes.string,
    onInputChange: PropTypes.func.isRequired,
    className: PropTypes.string
};

TextBox.defaultProps = {
    placeholder: 'Please add value',
    className: ''
};

export default TextBox;
