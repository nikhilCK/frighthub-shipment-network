import React, {Component} from "react";
import PropTypes from 'prop-types';

import "./styles.scss"


class Toast extends Component{
    constructor(props) {
        super(props);
        this.state = {
            show : false
        }
    }

    showMessage = () => {
        this.setState({
            show: true
        });
        setTimeout(() => {
            this.setState({
                show: false
            })
        }, this.props.timeout);
    };
    render() {
        return (
            <div id="snackbar" className={this.state.show ? 'show' : ''}>{this.props.message}</div>
        );
    }
}

Toast.propTypes = {
    message: PropTypes.string.isRequired,
    timeout: PropTypes.number
};
Toast.defaultProps = {
    timeout: 3000
};

export default Toast;
