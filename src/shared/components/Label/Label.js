import React from "react";
import PropTypes from 'prop-types';

import "./styles.scss"


const Label = (props) => {
    return (
        <span className="tag">
            {props.left ? <span className="tag-text">{props.left}</span> : null }
            {props.text ? <label className="tag-label ">{props.text}</label> : null}
        </span>
    );
};


Label.propTypes = {
    left: PropTypes.any,
    text: PropTypes.string
};
Label.defaultProps = {
    text: ''
};

export default Label;
