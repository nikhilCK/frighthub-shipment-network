

import React from 'react';
import './styles.scss';

const HeaderBar = (props) => {
  return (
      <header className="page-header">
        <div className="hdr-content">
          <div className="section-1">
            <span className="title">{props.title}</span>
          </div>
        </div>
      </header>
  )
};

export default HeaderBar;
