/* core dependencies */
import React from 'react';
import PropTypes from "prop-types";

/* styles */
import './styles.scss';

const Dropdown = (props) => {
    const selectionChange = (event) => {
        props.onSelectionDone(event.target.value);
    };
    let optionView = props.options.map(item => {
        return <option key={item.value} value={item.value}>{item.label}</option>
    });
    return (
        <select className='select-css' style={props.customStyle} onChange={selectionChange} value={props.selectedValue}>
            {optionView}
        </select>
    )
};

Dropdown.propTypes = {
    onSelectionDone: PropTypes.func.isRequired,
    options: PropTypes.array.isRequired,
    selectedValue: PropTypes.string.isRequired,
    customStyle: PropTypes.object,
};

Dropdown.defaultProps = {
    customStyle: {
        width: '150px'
    }
};
export default Dropdown;
