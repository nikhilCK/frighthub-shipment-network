import React from "react";
import PropTypes from "prop-types";

import './styles.scss';

const SimpleTable = (props) => {

    // Data
    const dataColumns = props.columns;
    const dataRows = props.rows;

    let tableHeaders = (
        <thead>
            <tr key={'col-header'}>
                {dataColumns.map(function (column) {
                    return (<th key={column}>{column}</th>);
                })}
            </tr>
        </thead>
    );
    const handleRowClick = (event) => {
        if (typeof props.onRowClick === 'function') {
            props.onRowClick(event);
        }
    };

    let tableBody = dataRows ? dataRows.map((row) => {
        return (
            <tr key={row.id} onClick={() => handleRowClick(row)} >
                {dataColumns.map(function (column) {
                    return <td title={row[column].length > 15 ? row[column] : ''} key={row[column]}>{row[column]}</td>;
                })}
            </tr>);
    }) : null;

    return (
        <table
            id='simple-table'
            style={props.customStyle}
        >
            {tableHeaders}
            <tbody >
                {tableBody}
            </tbody>
        </table>
    )
};

SimpleTable.propTypes = {
    onRowClick: PropTypes.func,
    columns: PropTypes.array.isRequired,
    rows: PropTypes.array,
    customStyle: PropTypes.object
};
SimpleTable.defaultProps = {
    customStyle : {}
};

export default SimpleTable;
