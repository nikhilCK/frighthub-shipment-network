
import React from "react";
import PropTypes from 'prop-types';

import "./styles.scss"


const ButtonSolid = (props) => {

  const onClick = (e) => {
    e.preventDefault();
    e.target.blur();
    props.onClick();
  };

  return (
    <button
        className="btn-solid"
        onClick={onClick}
        style={props.customStyle}
        disabled={props.isDisabled}
      >
        {props.title}
      </button>
  );
};



ButtonSolid.propTypes = {
  onClick: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  customStyle: PropTypes.object,
  isDisabled: PropTypes.bool
};
ButtonSolid.defaultProps = {
  customStyle: {
    width: '100px',
    height: '40px'
  }
};
export default ButtonSolid;
