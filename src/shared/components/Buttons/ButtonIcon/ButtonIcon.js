
import React from "react";
import PropTypes from 'prop-types';

import "./styles.scss"


const ButtonIcon = (props) => {

  const onClick = (e) => {
    e.preventDefault();
    e.target.blur();
    props.onClick();
  };

  return (
    <div className="button-wrpr">
      <button
        className="button-accent"
        onClick={onClick}
        style={props.customStyle}
        disabled={props.isDisabled}
      >
        {props.icon}
      </button>
        { props.title ? <label>{props.title}</label> : null }
    </div>

  );
};



ButtonIcon.propTypes = {
  onClick: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  customStyle: PropTypes.object,
  isDisabled: PropTypes.bool
};
ButtonIcon.defaultProps = {
  customStyle: {
    width: '100%'
  },
  title: ''
};
export default ButtonIcon;
