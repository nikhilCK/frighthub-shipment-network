import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { debounce } from '../../../core/utlity';
import './styles.scss';
// import {debounce} from "../../../core/utlity/debounce";

class SearchBar extends Component {

  constructor(props) {
    super(props);

    this.state = {
      constraint: ''
    };

    this.style = {
      "width": this.props.width
    };

    this.wrapperClasses = "search-field " + this.props.className;
    this.goSearch = debounce(this.goSearch, 1000)
  }

  updateInputValue = (evt) => {
    this.setState({
      constraint: evt.target.value
    }, () => {
      this.goSearch();
    });
  };

  onKeyUp = (evt) => {
    if (evt.keyCode === 13) {
      this.goSearch();
    }
  };

  goSearch = () => {
    this.props.onSearch(this.state.constraint);
  };

  render() {
    let { constraint } = this.state;
    const { placeholder } = this.props;
    return (
      <div className={this.wrapperClasses} style={this.style}>
        <span>
          <FontAwesomeIcon icon="search" />
        </span>
        <input type="input"
          className="search-box-input"
          placeholder={ placeholder ? placeholder : 'Start typing...'}
          value={constraint}
          onChange={this.updateInputValue}
          onKeyUp={this.onKeyUp}
        />
      </div>
    );
  }
}

export default SearchBar;
