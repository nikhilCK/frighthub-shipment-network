/* core dependencies */
import React, {Component} from "react";
import {Router, Route, Switch} from 'react-router-dom';
import * as historyLib from 'history';

/* components */
import ShipmentList from "./scenes/shipmentlist/ShipmentList";
import ShipmentDetails from "./scenes/shipmentdetails/ShipmentDetails";
import HeaderBar from "./shared/components/HeaderBar/HeaderBar";

/* styles */
import './style.scss';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faSearch, faSave } from '@fortawesome/free-solid-svg-icons';
library.add(faSearch);
library.add(faSave);

const history = historyLib.createBrowserHistory();

class App extends Component {
    render() {
        let message = 'FrightHub Shipping Network';
        return (
            <main className="app-container">
                <HeaderBar title={message} />
                <Router history={history}>
                    <Switch>
                        <Route exact path="/" component={ShipmentList} />
                        <Route path="/details/:shipmentId" component={ShipmentDetails} />
                    </Switch>
                </Router>
            </main>
        )
    }
}

export default App;
